window.onload = function () {
	//////////SLIDER FOR SECTION WORKS///////////////////
	var i = 0; 
	var images = document.querySelectorAll(".slider-body img")
	document.querySelector('.prev').onclick = function () {
		images[i].classList.remove('show');
		i--; 
		if(i < 0) {
			i = images.length - 1;
		}
		images[i].classList.add('show');
	}
	document.querySelector('.next').onclick = function () {
		images[i].classList.remove('show');
		i++; 
		if(i >= images.length) {
			i=0;
		}
		images[i].classList.add('show');
	}
}//onload




